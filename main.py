import mido
import time
from threading import Event, Thread

def clock_counter(clock:Event, beats):
    ticks = beats * 24
    global current_tick
    current_tick = 0
    while True:
        for tick in range(ticks):
            clock.wait()
            current_tick = tick
            clock.clear()

def clock_reader(clock:Event):
    with mido.open_input('KAOSSILATOR PRO 1 PAD 0') as inport:
        for msg in inport:
            if msg.type == 'clock':
                clock.set()
            else:
                print("msg is no clock")

clock = Event()
beats = 8
counter = Thread(target=clock_counter, args=(clock, beats))
reader = Thread(target=clock_reader, args=(clock, ))

counter.start()
reader.start()
tick = 0
with mido.open_output('KAOSSILATOR PRO 1 SOUND 1') as outport:
    beat_loop = 0
    while True:
        if not tick == current_tick:
            tick = current_tick
            print(tick)
            if beat_loop < beats:
                if tick % 24 == 0:
                    #if beat_loop == 0:
                    #    loop_a = mido.Message('note_on', note=36)
                    #    outport.send(loop_a)
                    #    loop_a = mido.Message('note_off', note=36)
                    #    outport.send(loop_a)
                    padx = mido.Message('control_change', control=12, value=0)
                    outport.send(padx)
                    pady = mido.Message('control_change', control=13, value=100)
                    outport.send(pady)
                    padt = mido.Message('control_change', control=92)
                    outport.send(padt.copy(value=1))
                    outport.send(padt.copy(value=0))
                    #if beat_loop == 15:
                    #    loop_a = mido.Message('note_on', note=36)
                    #    outport.send(loop_a)
                    #    loop_a = mido.Message('note_off', note=36)
                    #    outport.send(loop_a)
                    beat_loop = beat_loop + 1
                    



    padt = mido.Message('control_change', control=92)
    padx = mido.Message('control_change', control=12)
    pady = mido.Message('control_change', control=13)
    outport.send(padt.copy(value=1))
    for x in range(128):
        outport.send(padx.copy(value=x))
        for y in range(128):
            outport.send(pady.copy(value=y))
            time.sleep(0.05)
    print()